using System;


namespace API.Monitor.Test
{
    /// <summary>應用程式上下文環境
    /// </summary>
    public static class AppContext
    {
        /// <summary>業務邏輯服務定位器容器
        /// </summary>
        /// <value></value>
        public static IServiceProvider ServiceLocator { get; set; }

        /// <summary>RemoteProxy 在 runtime 下的 serviceClientConfig.json 設定檔的完整路徑。
        /// </summary>
        /// <value></value>
        public static string ServiceClientConfigPath { get; set; }
    }
}