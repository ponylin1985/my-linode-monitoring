using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;


namespace API.Monitor.Test
{
    /// <summary>測試用資料載體
    /// </summary>
    public class MagicDTO
    {
        /// <summary>測試用的訊息
        /// </summary>
        [JsonProperty( PropertyName = "magicWords" )]
        [JsonPropertyName( "magicWords" )]
        public string MagicWords { get; set; }

        /// <summary>測試日期
        /// </summary>
        [JsonProperty( PropertyName = "magicTime" )]
        [JsonPropertyName( "magicTime" )]
        public DateTime MagicTime { get; set; }

        /// <summary>測試用 Double 浮點數
        /// </summary>
        [JsonProperty( PropertyName = "testDouble", NullValueHandling = NullValueHandling.Ignore )]
        [JsonPropertyName( "testDouble" )]
        public double? TestDouble { get; set; }
    }
}