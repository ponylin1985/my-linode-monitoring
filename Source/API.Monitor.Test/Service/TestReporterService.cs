using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZayniFramework.Common;


namespace API.Monitor.Test
{
    /// <summary>測試報告服務
    /// </summary>
    public class TestReporterService : ITestReporterService
    {
        /// <summary>產生測試報告
        /// * 需要搭配標記 `[MyTestMethod()]` 測試方法屬性才會正確生效。
        /// * `IResult.Success`: 代表測試是否成功
        /// * `IResult.Data`: 代表測試報告訊息
        /// </summary>
        /// <param name="testClassName">測試類別名稱</param>
        /// <param name="beginTime">測試起始時間</param>
        /// <param name="endTime">測試結束時間</param>
        /// <returns>測試報告
        /// * `IResult.Success`: 代表測試是否成功
        /// * `IResult.Data`: 代表測試報告訊息
        /// </returns>
        public IResult<string> Report( string testClassName, DateTime beginTime, DateTime endTime ) 
        {
            var rst = Result.Create<string>();
            var failures = new List<object>();

            endTime = DateTime.UtcNow;

            bool debug = false;

            if ( ZayniConfigManagement.AppSettings.TryGetValue( "DisplayOutput", out var strOutput ) )
            {
                debug = bool.TryParse( strOutput, out var displayOutput ) ? displayOutput : false;
            }

            var sb = new StringBuilder();

            foreach ( var result in TestResultCollection.Results.Where( m => m.Key.TestClassName == testClassName ) )
            {
                var testMethod  = result.Key;
                var testResults = result.Value;

                var sbResults = new StringBuilder();

                foreach ( var testResult in testResults )
                {
                    if ( testResult.Outcome != UnitTestOutcome.Passed )
                    {
                        failures.Add( testResult );
                    }

                    sbResults.AppendLine( $"- Test Result: {testResult.Outcome}, Test Duration: {testResult.Duration.TotalMilliseconds} ms." );
                    
                    if ( debug )
                    {
                        sbResults.AppendLine( $"    Test Output:" );
                        sbResults.AppendLine( $"{testResult.LogOutput.IsNullOrEmptyString( testResult.DebugTrace )}" );
                    }
                }

                sb.AppendLine( $"- Test Name: {testMethod.TestMethodName}." );
                sb.AppendLine( $"    {sbResults.ToString()}" );
            }

            var duration  = ( endTime - beginTime ).TotalMilliseconds;
            var sbMessage = new StringBuilder();
            sbMessage.AppendLine( $"{Environment.NewLine}{Environment.NewLine}" );
            sbMessage.AppendLine( $"Test Class: {testClassName}{Environment.NewLine}" );
            sbMessage.AppendLine( $"{sb.ToString()}{Environment.NewLine}" );
            sbMessage.AppendLine( $"Total Duration {duration} ms." );

            rst.Data = sbMessage.ToString();

            if ( failures.IsNotNullOrEmpty() )
            {
                return rst;
            }

            rst.Success = true;
            return rst;
        }
    }
}