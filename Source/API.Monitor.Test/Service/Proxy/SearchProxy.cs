namespace API.Monitor.Test
{
    /// <summary>數位資產搜尋服務代理人介面
    /// </summary>
    public class SearchProxy : ServiceProxy, ISearchProxy
    {
        /// <summary>預設建構子
        /// </summary>
        public SearchProxy() : base( serviceClientName: "DigitalAssets.Searching.Client" )
        {
        }
    }
}