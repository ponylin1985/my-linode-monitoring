namespace API.Monitor.Test
{
    /// <summary>數位資產管理服務代理人類別
    /// </summary>
    public class AssetMgtProxy : ServiceProxy, IAssetMgtProxy
    {
        /// <summary>預設建構子
        /// </summary>
        public AssetMgtProxy() : base( serviceClientName: "DigitalAssets.AssetMgt.Client" )
        {
        }
    }
}