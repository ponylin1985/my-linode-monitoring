namespace API.Monitor.Test
{
    /// <summary>數位資產資料存取服務代理人
    /// </summary>
    public class DataAccessProxy : ServiceProxy, IDataAccessProxy
    {
        /// <summary>預設建構子
        /// </summary>
        public DataAccessProxy() : base( serviceClientName: "DigitalAssets.DataAccess.Client" )
        {
        }
    }
}