namespace API.Monitor.Test
{
    /// <summary>標籤管理服務代理人類別
    /// </summary>
    public class TagMgtProxy : ServiceProxy, ITagMgtProxy
    {
        /// <summary>預設建構子
        /// </summary>
        public TagMgtProxy() : base( serviceClientName: "DigitalAssets.TagMgt.Client" )
        {
        }
    }
}