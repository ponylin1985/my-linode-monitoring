using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Client;


namespace API.Monitor.Test
{
    /// <summary>遠端代理人類別
    /// </summary>
    public abstract class ServiceProxy : RemoteProxy, IRemoteProxy
    {
        /// <summary>多載建構子
        /// </summary>
        public ServiceProxy( string serviceClientName ) : 
            base( serviceClientName: serviceClientName, configPath: AppContext.ServiceClientConfigPath )
        {
        }

        /// <summary>執行遠端服務呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public new async Task<Result<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>( string actionName, TReqDTO request ) => 
            await base.ExecuteAsync<TReqDTO, TResDTO>( actionName, request );

        /// <summary>執行遠端服務呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public new async Task<Result> ExecuteAsync<TReqDTO>( string actionName, TReqDTO request ) => 
            await base.ExecuteAsync<TReqDTO>( actionName, request );
    }
}