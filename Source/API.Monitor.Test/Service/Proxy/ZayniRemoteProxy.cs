namespace API.Monitor.Test
{
    /// <summary>`WebAPI.Test.HttpCore` Service Host 的遠端代理人類別
    /// </summary>
    public class ZayniApiProxy : ServiceProxy, IZayniRemoteProxy
    {
        /// <summary>預設建構子
        /// </summary>
        public ZayniApiProxy() : base( serviceClientName: "ZayniFramework.WebAPI.Test.Client" )
        {
        }
    }
}