using Microsoft.Extensions.DependencyInjection;
using System;


namespace API.Monitor.Test
{
    /// <summary>遠端服務代理人工廠
    /// </summary>
    public class ProxyFactory : IProxyFactory
    {
        /// <summary>建立遠端服務代理人
        /// </summary>
        /// <param name="type">代理人種類</param>
        /// <returns>遠端代理人物件</returns>
        public IRemoteProxy Create( ProxyType type ) 
        {
            switch ( type )
            {
                case ProxyType.Zayni:
                    return AppContext.ServiceLocator.GetService<IZayniRemoteProxy>();

                case ProxyType.AssetMgt:
                    return AppContext.ServiceLocator.GetService<IAssetMgtProxy>();

                case ProxyType.TagMgt:
                    return AppContext.ServiceLocator.GetService<ITagMgtProxy>();

                case ProxyType.Searching:
                    return AppContext.ServiceLocator.GetService<ISearchProxy>();

                case ProxyType.DataAccess:
                    return AppContext.ServiceLocator.GetService<IDataAccessProxy>();

                default:
                    throw new Exception();
            }
        }
    }

    /// <summary>代理人種類
    /// </summary>
    public enum ProxyType 
    {
        /// <summary>ZayniFramework 的 WebAPI.Test 測試服務
        /// </summary>
        Zayni,

        /// <summary>DigitalAssets 數位資產的數位資產管理服務
        /// </summary>
        AssetMgt,

        /// <summary>DigitalAssets 數位資產的標籤管理服務
        /// </summary>
        TagMgt,

        /// <summary>DigitalAssets 數位資產的搜尋服務
        /// </summary>
        Searching,

        /// <summary>DigitalAssets 數位資產的資料存取服務
        /// </summary>
        DataAccess
    }
}