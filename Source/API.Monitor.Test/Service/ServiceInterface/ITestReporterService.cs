using System;
using ZayniFramework.Common;


namespace API.Monitor.Test
{
    /// <summary>測試報告服務介面
    /// </summary>
    public interface ITestReporterService
    {
        /// <summary>產生測試報告
        /// * `IResult.Success`: 代表測試是否成功
        /// * `IResult.Data`: 代表測試報告訊息
        /// </summary>
        /// <param name="testClassName">測試類別名稱</param>
        /// <param name="beginTime">測試起始時間</param>
        /// <param name="endTime">測試結束時間</param>
        /// <returns>測試報告
        /// * `IResult.Success`: 代表測試是否成功
        /// * `IResult.Data`: 代表測試報告訊息
        /// </returns>
        IResult<string> Report( string testClassName, DateTime beginTime, DateTime endTime );
    }
}