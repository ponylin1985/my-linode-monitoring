using System.Threading.Tasks;
using ZayniFramework.Common;


namespace API.Monitor.Test
{
    /// <summary>遠端代理人介面
    /// </summary>
    public interface IRemoteProxy 
    {
        /// <summary>執行遠端服務呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        Task<Result<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>( string actionName, TReqDTO request );

        /// <summary>執行遠端服務呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        Task<Result> ExecuteAsync<TReqDTO>( string actionName, TReqDTO request );
    }
}