namespace API.Monitor.Test
{
    /// <summary>
    /// </summary>
    public interface IProxyFactory
    {
        /// <summary>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IRemoteProxy Create( ProxyType type );
    }
}