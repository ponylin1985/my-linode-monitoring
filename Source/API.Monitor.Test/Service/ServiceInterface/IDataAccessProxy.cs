namespace API.Monitor.Test
{
    /// <summary>數位資產資料存取服務代理人介面
    /// </summary>
    public interface IDataAccessProxy : IRemoteProxy
    {
    }
}