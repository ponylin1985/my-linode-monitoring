using System.Diagnostics.Tracing;


namespace API.Monitor.Test
{
    /// <summary>訊息通知服務介面
    /// </summary>
    public interface INotificationService
    {
        /// <summary>發送日誌通知
        /// </summary>
        /// <param name="level">事件層級</param>
        /// <param name="title">日誌事件標題</param>
        /// <param name="message">日誌事件訊息</param>
        void Notify( EventLevel level, string title, string message );
    }
}