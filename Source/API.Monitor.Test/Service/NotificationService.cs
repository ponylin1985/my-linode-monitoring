using System;
using System.Diagnostics.Tracing;
using ZayniFramework.Logging;


namespace API.Monitor.Test
{
    /// <summary>訊息通知服務
    /// </summary>
    public class NotificationService : INotificationService
    {
        /// <summary>日誌訊息通知的 Logger 設定名稱
        /// </summary>
        private readonly string _notifyLoggerName = "MailLog";

        /// <summary>發送日誌通知
        /// </summary>
        /// <param name="level">事件層級</param>
        /// <param name="title">日誌事件標題</param>
        /// <param name="message">日誌事件訊息</param>
        public void Notify( EventLevel level, string title, string message ) 
        {
            Action<object, string, string, string> action = null;

            switch ( level )
            {
                case EventLevel.Informational:
                default:
                    action = Logger.Info;
                    break;

                case EventLevel.Error:
                case EventLevel.Critical:
                    action = Logger.Error;
                    break;
            }

            action( this, message, title, _notifyLoggerName );
        }
    }
}