using CArk.Topline.DigitalAssets.AssetMgt.Proxy;
using CArk.Topline.DigitalAssets.Searching.Proxy;
using CArk.Topline.DigitalAssets.TagMgt.Proxy;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Grpc.Client;
using ZayniFramework.Middle.Service.HttpCore.Client;


namespace API.Monitor.Test
{
    /// <summary>測試初始化類別
    /// </summary>
    [TestClass()]
    public class InitTest
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init( TestContext testcontext ) 
        {
            InitZayniFramework();
            InitServiceIoCContainer();
        }

        #endregion 測試組件初始化


        #region Private Methods

        /// <summary>初始化 ZayniFramework 框架
        /// </summary>
        private static void InitZayniFramework() 
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 zayni.config 的完整絕對路徑到 ZayniConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var execPath = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );
            var path     = $"{execPath}{Path.DirectorySeparatorChar}conf{Path.DirectorySeparatorChar}zayni.config";
            
            ZayniConfigManagement.Configure( path );
            AppContext.ServiceClientConfigPath = $"{execPath}{Path.DirectorySeparatorChar}conf{Path.DirectorySeparatorChar}serviceClientConfig.json";

            var g = RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" );

            if ( !g.Success )
            {
                throw new Exception( $"An error occured while register the '{nameof ( gRPCRemoteClient )}' to ZayniFramework.{Environment.NewLine}{g.Message}" );
            }

            var h = RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>( "HttpCore" );

            if ( !h.Success )
            {
                throw new Exception( $"An error occured while register the '{nameof ( HttpCoreRemoteClient )}' to ZayniFramework.{Environment.NewLine}{h.Message}" );
            }
        }

        /// <summary>初始化單元測試內部使用的服務定位器容器
        /// </summary>
        private static void InitServiceIoCContainer() 
        {
            AppContext.ServiceLocator = 
                new ServiceCollection()
                    .AddTransient<ITestReporterService, TestReporterService>()
                    .AddTransient<INotificationService, NotificationService>()
                    .AddTransient<IProxyFactory, ProxyFactory>()
                    .AddTransient<IZayniRemoteProxy, ZayniApiProxy>()
                    .AddTransient<IAssetMgtProxy, AssetMgtProxy>()
                    .AddTransient<ITagMgtProxy, TagMgtProxy>()
                    .AddTransient<ISearchProxy, SearchProxy>()
                    .AddTransient<IDataAccessProxy, DataAccessProxy>()
                    .AddTransient<IAssetProxy, AssetProxy>()
                    .AddTransient<ITagProxy, TagProxy>()
                    .AddTransient<ISearchEngineProxy, SearchEngineProxy>()
                    .BuildServiceProvider();
        }

        #endregion Privete Methods
    }
}