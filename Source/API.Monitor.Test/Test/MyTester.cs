using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics.Tracing;
using ZayniFramework.Common;


namespace API.Monitor.Test
{
    /// <summary>單元測試類別
    /// </summary>
    public abstract class MyTester
    {
        #region Protected Properties

        /// <summary>服務定位器容器
        /// </summary>
        /// <value></value>
        protected static IServiceProvider ServiceContainer { get; set; }

        /// <summary>測試起始時間
        /// </summary>
        protected static DateTime StartTime { get; set; }

        /// <summary>測試結束時間
        /// </summary>
        protected static DateTime EndTime { get; set; }

        /// <summary>測試結果
        /// * `IResult.Success`: 代表測試是否成功
        /// * `IResult.Data`: 代表測試報告訊息
        /// </summary>
        protected static IResult<string> Report { get; set; }

        /// <summary>通知訊息標題
        /// </summary>
        protected static string NotificationTilte { get; set; }

        #endregion Protected Properties


        #region Protected Methods

        /// <summary>產生測試結果
        /// </summary>
        /// <param name="testClassName">測試類別名稱</param>
        protected static void GetTestReport( string testClassName ) 
        {
            var service = ServiceContainer.GetService<ITestReporterService>();
            var report  = service.Report( testClassName, StartTime, EndTime );
            Report      = report;
        }

        /// <summary>發送日誌通知
        /// </summary>
        protected static void Notify() 
        {
            var service    = ServiceContainer.GetService<INotificationService>();
            var eventLevel = Report.Success ? EventLevel.Informational : EventLevel.Error;
            service.Notify( eventLevel, NotificationTilte, Report.Data );
        }

        #endregion Protected Methods
    }
}