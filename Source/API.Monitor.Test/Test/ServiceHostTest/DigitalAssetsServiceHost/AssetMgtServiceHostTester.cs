using CArk.Topline.DigitalAssets.AssetMgt;
using CArk.Topline.DigitalAssets.AssetMgt.Entity;
using CArk.Topline.DigitalAssets.AssetMgt.Proxy;
using CArk.Topline.DigitalAssets.DataAccess.Entity;
using CArk.Topline.DigitalAssets.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;
using Proxy = ZayniFramework.Common.Proxy;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.AssetMgtServiceHostTester
    /// <summary>DigitalAssets.AssetMgt.Service 的 Service Host 測試類別
    /// </summary>
    [TestClass()]
    public class AssetMgtServiceHostTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>數位資產服務代理人 (nuget package library...)
        /// </summary>
        private static IAssetProxy _assetProxy;

        /// <summary>數位資產服務代理人 (在此專案重新撰寫的...)
        /// </summary>
        private static IRemoteProxy _remoteProxy;

        /// <summary>AssetMgt.Service ASP.NET Core API 的 Access Token 字串
        /// </summary>
        private static string _accessToken;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets AssetMgt.Service HttpCore Service Host.";
            _assetProxy       = ServiceContainer.GetService<IAssetProxy>();
            _remoteProxy      = ServiceContainer.GetService<IProxyFactory>().Create( ProxyType.AssetMgt );
            _accessToken      = ZayniConfigManagement.AppSettings[ "DigitalAssets-AssetMgt.Service-AccessToken" ];
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( AssetMgtServiceHostTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>GetAssets 查詢數位資產列表的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "GetAssets 查詢數位資產列表的測試" )]
        public async Task GetAssets_Test()
        {
            var request = new GetAssetsReqDTO()
            {
                PageNo      = 1,
                PageSize    = 10,
                AccessToken = $"Bearer {_accessToken}"
            };

            var g = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _remoteProxy.ExecuteAsync<GetAssetsReqDTO, CollectionPagingResDTO<AssetDTO>>( $"{AppConstants.ActionPrefix}GetAssets", request );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( g.Success );
            Assert.AreEqual( StatusConstants.SUCCESS, g.Code );

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _assetProxy.GetAssetsAsync( request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>GetAsset 查詢數位資產明細的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "GetAsset 查詢數位資產明細的測試" )]
        public async Task GetAsset_Test()
        {
            var request = new GetAssetReqDTO()
            {
                Id          = "1",
                AccessToken = $"Bearer {_accessToken}"
            };

            var g = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _remoteProxy.ExecuteAsync<GetAssetReqDTO, AssetDTO>( $"{AppConstants.ActionPrefix}GetAsset", request );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( g.Success );
            Assert.AreEqual( StatusConstants.SUCCESS, g.Code );

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _assetProxy.GetAssetAsync( request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Test Methods
    }
}