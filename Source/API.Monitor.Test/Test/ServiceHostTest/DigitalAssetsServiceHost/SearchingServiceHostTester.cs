using CArk.Topline.DigitalAssets.Searching;
using CArk.Topline.DigitalAssets.Searching.Entity;
using CArk.Topline.DigitalAssets.Searching.Proxy;
using CArk.Topline.DigitalAssets.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;
using Proxy = ZayniFramework.Common.Proxy;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.SearchingServiceHostTester
    /// <summary>DigitalAssets.Searching.Service 的 Service Host 測試類別
    /// </summary>
    [TestClass()]
    public class SearchingServiceHostTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>搜尋服務代理人 (nuget package library...)
        /// </summary>
        private static ISearchEngineProxy _searchProxy;

        /// <summary>搜尋服務代理人 (在此專案重新撰寫的...)
        /// </summary>
        private static IRemoteProxy _remoteProxy;

        /// <summary>Searching.Service ASP.NET Core API 的 Access Token 字串
        /// </summary>
        private static string _accessToken;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets Searching.Service HttpCore Service Host.";
            _searchProxy      = ServiceContainer.GetService<ISearchEngineProxy>();
            _remoteProxy      = ServiceContainer.GetService<IProxyFactory>().Create( ProxyType.Searching );
            _accessToken      = ZayniConfigManagement.AppSettings[ "DigitalAssets-Searching.Service-AccessToken" ];
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( SearchingServiceHostTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>Search 搜尋數位資產的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "Search 搜尋數位資產的測試" )]
        public async Task Search_Test()
        {
            var request = new SearchReqDTO()
            {
                AssetType   = "PHOTO",
                PageNo      = 1,
                PageSize    = 10,
                AccessToken = $"Bearer {_accessToken}"
            };

            var g = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _remoteProxy.ExecuteAsync<SearchReqDTO, CollectionPagingResDTO<object>>( $"{AppConstants.ActionPrefix}Search", request );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( g.Success );
            Assert.AreEqual( StatusConstants.SUCCESS, g.Code );

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _searchProxy.SearchAsync( request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>關鍵字搜尋測試
        /// </summary>
        [MyTestMethod()]
        [Description( "關鍵字搜尋測試" )]
        public async Task SearchByKeyword_API_Test()
        {
            var request = new AssetKeywordSearchReqDTO()
            {
                Keyword     = "WB",
                AccessToken = $"Bearer {_accessToken}"
            };

            var g = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _remoteProxy.ExecuteAsync<AssetKeywordSearchReqDTO, CollectionPagingResDTO<AssetDTO>>( $"{AppConstants.ActionPrefix}SearchAssetsByKeyword", request );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( g.Success );
            Assert.AreEqual( StatusConstants.SUCCESS, g.Code );

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _searchProxy.SearchAsync( request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Test Methods
    }
}