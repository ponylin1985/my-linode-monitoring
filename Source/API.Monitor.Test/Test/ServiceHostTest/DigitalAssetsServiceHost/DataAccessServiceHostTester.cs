using CArk.Topline.DigitalAssets.DataAccess;
using CArk.Topline.DigitalAssets.DataAccess.Entity;
using CArk.Topline.DigitalAssets.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.DataAccessServiceHostTester
    /// <summary>DigitalAssets.DataAccess.Service 的 Service Host 測試類別
    /// </summary>
    [TestClass()]
    public class DataAccessServiceHostTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>數位資產資料存取服務代理人 (在此專案重新撰寫的...)
        /// </summary>
        private static IRemoteProxy _remoteProxy;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets DataAccess.Service HttpCore Service Host.";
            _remoteProxy      = ServiceContainer.GetService<IProxyFactory>().Create( ProxyType.DataAccess );
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( DataAccessServiceHostTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>GetPhotosAmbiguous 查詢「尚未校正」的傢俱圖片資料列表的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "GetPhotosAmbiguous 查詢「尚未校正」的傢俱圖片資料列表的測試" )]
        public async Task GetPhotosAmbiguous_Test()
        {
            var request = new GetPhotosReqDTO()
            {
                PageNo      = 1,
                PageSize    = 10
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _remoteProxy.ExecuteAsync<GetPhotosReqDTO, CollectionPagingResDTO<DropboxFileModel>>( $"{AppConstants.ActionPrefix}GetPhotosAmbiguous", request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
                Assert.IsTrue( g.Data.Collection.IsNotNullOrEmpty() );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>GetDropboxPhotoDetail 查詢傢俱照片在 Dropbox 的明細的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "GetDropboxPhotoDetail 查詢傢俱照片在 Dropbox 的明細的測試" )]
        public async Task GetDropboxPhotoDetail_Test()
        {
            var request = new GetAssetDetailReqDTO()
            {
                Id = 1
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _remoteProxy.ExecuteAsync<GetAssetDetailReqDTO, DropboxFileModel>( $"{AppConstants.ActionPrefix}GetDropboxPhotoDetail", request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>Echo 回聲測試
        /// </summary>
        [MyTestMethod()]
        [Description( "Echo 回聲測試" )]
        public async Task Echo_Test()
        {
            var request = new FooReqDTO();

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var q = await _remoteProxy.ExecuteAsync<FooReqDTO>( $"{AppConstants.ActionPrefix}EchoAsync", request );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( q ), ConsoleColor.Magenta );
                        return q;
                    },
                    retryOption: _retryOption
                );

                Assert.IsTrue( g.Success );
                Assert.AreEqual( StatusConstants.SUCCESS, g.Code );
                Assert.IsTrue( g.Message.IsNotNullOrEmpty() );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Test Methods
    }
}