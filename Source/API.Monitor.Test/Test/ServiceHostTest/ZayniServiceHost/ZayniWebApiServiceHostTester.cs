﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.ZayniWebApiServiceHostTester
    /// <summary>ZayniFramework WebAPI.Test 的 Service Host 測試類別
    /// </summary>
    [TestClass()]
    public class ZayniWebApiServiceHostTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>
        /// </summary>
        private static IRemoteProxy _proxy;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] ZayniFramework WebAPI.Test.HttpCore.";
            _proxy            = ServiceContainer.GetService<IProxyFactory>().Create( ProxyType.Zayni );
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( ZayniWebApiServiceHostTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>RemoteProxy 遠端代理人執行 RPC 請求的測試
        /// </summary>
        [MyTestMethod()]
        [Description( "RemoteProxy 遠端代理人執行 RPC 請求的測試" )]
        public async Task RemoteProxy_RPC_Test()
        {
            var json   = default ( string );
            var random = new Random( Guid.NewGuid().GetHashCode() );
            Assert.IsNotNull( _proxy );

            for ( int i = 0; i < 5; i++ )
            {
                var request = new SomethingDTO()
                {
                    SomeMessage = "From RemoteProxyTester unit test.",
                    LuckyNumber = random.Next( 0, 2000 )
                };

                
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var r = await _proxy.ExecuteAsync<SomethingDTO, MagicDTO>( "FooActionAsync", request );
                        json = JsonConvertUtil.Serialize( r );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                        return r;
                    },
                    retryOption: _retryOption
                );

                await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.FooActionAsync response.{Environment.NewLine}{json}" );
                Debug.Print( $"RemoteProxy_RPC_Test.FooActionAsync response.{Environment.NewLine}{json}" );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.Data.IsNotNull() );
            }

            await Task.Delay( 10 );

            var reqCreate1 = new UserDTO()
            {
                Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
                Sex     = 0,
                Age     = 25,
                DoB     = new DateTime( 1998, 3, 27 ),
                IsGood  = true,
                IsVip   = true
            };

            var reqCreate2 = new UserDTO()
            {
                Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
                Sex     = 0,
                Age     = 27,
                DoB     = new DateTime( 1992, 3, 27 ),
                IsGood  = false,
                IsVip   = true
            };

            var reqCreate3 = new UserDTO()
            {
                Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
                Sex     = 1,
                Age     = 64,
                DoB     = new DateTime( 1953, 6, 14 ),
                IsGood  = true,
                IsVip   = false
            };

            for ( int i = 0; i < 1; i++ )
            {
                var g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate1 );
                        json = JsonConvertUtil.Serialize( r );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                        return r;
                    },
                    retryOption: _retryOption
                );
                
                await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync1 response.{Environment.NewLine}{json}" );
                Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync1 response.{Environment.NewLine}{json}" );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.Data.IsNotNull() );

                g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate2 );
                        json = JsonConvertUtil.Serialize( r );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                        return r;
                    },
                    retryOption: _retryOption
                );

                await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync2 response.{Environment.NewLine}{json}" );
                Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync2 response.{Environment.NewLine}{json}" );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.Data.IsNotNull() );

                g = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate3 );
                        json = JsonConvertUtil.Serialize( r );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                        return r;
                    },
                    retryOption: _retryOption
                );

                await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync3 response.{Environment.NewLine}{json}" );
                Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync3 response.{Environment.NewLine}{json}" );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.Data.IsNotNull() );
            }

            for ( int i = 0; i < 3; i++ )
            {
                var request = new UserDTO()
                {
                    Sex = 0
                };

                var z = await Proxy.InvokeAsync(
                    asyncFunc: async () =>
                    {
                        var r = await _proxy.ExecuteAsync<UserDTO, IList<UserDTO>>( "GetUserModelsActionAsync", request );
                        json = JsonConvertUtil.Serialize( r );
                        await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                        return r;
                    },
                    retryOption: _retryOption
                );

                await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelsActionAsync response.{Environment.NewLine}{json}" );
                Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelsActionAsync response.{Environment.NewLine}{json}" );

                Assert.IsTrue( z.Success );
                Assert.IsTrue( z.Data.IsNotNull() );
                await Task.Delay( 10 );
            }

            var q1 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate1.Name } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );
            
            var json1 = JsonConvertUtil.Serialize( q1 );
            await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync1 response.{Environment.NewLine}{json1}" );
            Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync1 response.{Environment.NewLine}{json1}" );
            Assert.IsTrue( q1.Success );
            Assert.IsNotNull( q1.Data );
            await Task.Delay( 10 );

            var q2 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate2.Name } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            var json2 = JsonConvertUtil.Serialize( q2 );
            await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync2 response.{Environment.NewLine}{json2}" );
            Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync2 response.{Environment.NewLine}{json2}" );
            Assert.IsTrue( q2.Success );
            Assert.IsNotNull( q2.Data );
            await Task.Delay( 10 );

            var q3 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate3.Name } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            var json3 = JsonConvertUtil.Serialize( q3 );
            await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync3 response.{Environment.NewLine}{json3}" );
            Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync3 response.{Environment.NewLine}{json3}" );
            Assert.IsTrue( q3.Success );
            Assert.IsNotNull( q3.Data );
            await Task.Delay( 10 );

            var d1 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q1.Data.Account } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( d1.Success );

            var d2 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q2.Data.Account } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( d2.Success );

            var d3 = await Proxy.InvokeAsync(
                asyncFunc: async () =>
                {
                    var r = await _proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q3.Data.Account } );
                    json = JsonConvertUtil.Serialize( r );
                    await CommandAsync.StdoutAsync( JsonConvertUtil.Serialize( r ), ConsoleColor.Magenta );
                    return r;
                },
                retryOption: _retryOption
            );

            Assert.IsTrue( d3.Success );
        }

        #endregion Public Test Methods
    }
}
