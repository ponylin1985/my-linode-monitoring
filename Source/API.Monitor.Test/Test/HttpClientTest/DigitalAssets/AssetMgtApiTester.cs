using CArk.Topline.DigitalAssets.AssetMgt.Entity;
using CArk.Topline.DigitalAssets.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.AssetMgtApiTester
    /// <summary>DigitalAssets.AssetMgt.Service.API 的測試類別
    /// </summary>
    [TestClass()]
    public class AssetMgtApiTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>AssetMgt.Service ASP.NET Core API 的 baseUrl 位址
        /// </summary>
        private static string _baseUrl;

        /// <summary>AssetMgt.Service ASP.NET Core API 的 Access Token 字串
        /// </summary>
        private static string _accessToken;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets AssetMgt.Service.API.";
            _baseUrl          = ZayniConfigManagement.AppSettings[ "DigitalAssets-AssetMgt.Service-BaseUrl" ];
            _accessToken      = ZayniConfigManagement.AppSettings[ "DigitalAssets-AssetMgt.Service-AccessToken" ];
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( AssetMgtApiTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Methods

        /// <summary>GetAssets 查詢數位資產列表的測試
        /// * GET `/asset?order={order}&amp;page_no={page_no}&amp;page_size={page_size}`
        /// </summary>
        [MyTestMethod()]
        [Description( "GetAssets 查詢數位資產列表的測試" )]
        public async Task GetAssets_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path    = "/asset",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                },
                QueryStrings =
                {
                    [ "order" ]     = "0",
                    [ "page_no" ]   = "1",
                    [ "page_size" ] = "10"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<CollectionPagingResDTO<AssetDTO>>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>GetAsset 查詢數位資產明細的測試
        /// * GET `/asset/{id}`
        /// </summary>
        [MyTestMethod()]
        [Description( "GetAsset 查詢數位資產明細的測試" )]
        public async Task GetAsset_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path    = "/asset/1",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<AssetDTO>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
                Assert.AreEqual( "1", r.Data.Id );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>GetConfig 查詢 API Service 的應用程式組態
        /// * GET `/configs`
        /// </summary>
        [MyTestMethod()]
        [Description( "GetConfig 查詢 API Service 的應用程式組態" )]
        public async Task GetConfig_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path    = "/configs",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( g.Data );
                Debug.Print( g.Data );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>Echo 回聲測試
        /// * GET `/echo?msg={msg}`
        /// </summary>
        [MyTestMethod()]
        [Description( "Echo 回聲測試" )]
        public async Task Echo_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path         = "/echo",
                QueryStrings =
                {
                    [ "msg" ] = "From GitLab CI Pipeline Monitor"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Methods
    }
}