using CArk.Topline.DigitalAssets.TagMgt.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.TagMgtApiTester
    /// <summary>DigitalAssets.TagMgt.Service.API 的測試類別
    /// </summary>
    [TestClass()]
    public class TagMgtApiTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>TagMgt.Service ASP.NET Core API 的 baseUrl 位址
        /// </summary>
        private static string _baseUrl;

        /// <summary>TagMgt.Service ASP.NET Core API 的 Access Token 字串
        /// </summary>
        private static string _accessToken;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets TagMgt.Service.API.";
            _baseUrl          = ZayniConfigManagement.AppSettings[ "DigitalAssets-TagMgt.Service-BaseUrl" ];
            _accessToken      = ZayniConfigManagement.AppSettings[ "DigitalAssets-TagMgt.Service-AccessToken" ];
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( TagMgtApiTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>GetTags 查詢標籤列表的測試
        /// * GET `/tag?phrase={phrase}`
        /// </summary>
        [MyTestMethod()]
        [Description( "GetTags 查詢標籤列表的測試" )]
        public async Task GetTags_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path    = "/tag",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                },
                QueryStrings =
                {
                    [ "phrase" ] = "LS"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<IEnumerable<TagDTO>>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
                Assert.IsTrue( r.Data.IsNotNullOrEmpty() );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>GetTag 查詢標籤列表的測試
        /// * GET `/tag/{tag}`
        /// </summary>
        [MyTestMethod()]
        [Description( "GetTag 查詢標籤列表的測試" )]
        public async Task GetTag_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var parameter = "LS1";

            var request = new HttpRequest()
            {
                Path    = "/tag/{tag}".Format( tag => parameter ),
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<TagDTO>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>Echo 回聲測試
        /// * GET `/echo?msg={msg}`
        /// </summary>
        [MyTestMethod()]
        [Description( "Echo 回聲測試" )]
        public async Task Echo_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path         = "/echo",
                QueryStrings =
                {
                    [ "msg" ] = "From GitLab CI Pipeline Monitor"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Test Methods
    }
}