using CArk.Topline.DigitalAssets.Searching.Entity;
using CArk.Topline.DigitalAssets.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.SearchingApiTester
    /// <summary>DigitalAssets.Searching.Service.API 的測試類別
    /// </summary>
    [TestClass()]
    public class SearchingApiTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        /// <summary>Searching.Service ASP.NET Core API 的 baseUrl 位址
        /// </summary>
        private static string _baseUrl;

        /// <summary>Searching.Service ASP.NET Core API 的 Access Token 字串
        /// </summary>
        private static string _accessToken;

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context )
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] DigitalAssets Searching.Service.API.";
            _baseUrl          = ZayniConfigManagement.AppSettings[ "DigitalAssets-Searching.Service-BaseUrl" ];
            _accessToken      = ZayniConfigManagement.AppSettings[ "DigitalAssets-Searching.Service-AccessToken" ];
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync()
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( SearchingApiTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>Search 搜尋數位資產的測試
        /// * POST `/search`
        /// </summary>
        [MyTestMethod()]
        [Description( "Search 搜尋數位資產的測試" )]
        public async Task Search_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var reqDTO = new SearchReqDTO()
            {
                AssetType = "DROPBOX-FILE"
            };

            var request = new HttpRequest()
            {
                Path    = "/search",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                },
                Body = JsonSerialize.SerializeObject( reqDTO )
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<CollectionPagingResDTO<object>>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>關鍵字搜尋測試
        /// * POST `/search/asset/keyword`
        /// </summary>
        [MyTestMethod()]
        [Description( "關鍵字搜尋測試" )]
        public async Task SearchByKeyword_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var reqDTO = new AssetKeywordSearchReqDTO()
            {
                Keyword = null
            };

            var request = new HttpRequest()
            {
                Path    = "/search/asset/keyword",
                Headers =
                {
                    [ "Authorization" ] = $"Bearer {_accessToken}"
                },
                Body = JsonSerialize.SerializeObject( reqDTO )
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );

                var r = JsonSerialize.DeserializeObject<Result<CollectionPagingResDTO<AssetDTO>>>( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( r.Success );
            } );

            await Task.FromResult( 0 );
        }

        /// <summary>Echo 回聲測試
        /// * GET `/echo?msg={msg}`
        /// </summary>
        [MyTestMethod()]
        [Description( "Echo 回聲測試" )]
        public async Task Echo_API_Test()
        {
            var httpClient = HttpClientFactory.Create( _baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var request = new HttpRequest()
            {
                Path         = "/echo",
                QueryStrings =
                {
                    [ "msg" ] = "From GitLab CI Pipeline Monitor"
                }
            };

            For.Reset();
            For.Do( 5, async () =>
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: true, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( $"{Environment.NewLine}API Response:{Environment.NewLine}{g.Data}" );
                Debug.Print( g.Data );
            } );

            await Task.FromResult( 0 );
        }

        #endregion Public Test Methods
    }
}