﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace API.Monitor.Test
{
    // dotnet test ./Source/API.Monitor.Test/API.Monitor.Test.csproj --filter ClassName=API.Monitor.Test.ZayniWebApiTester
    /// <summary>ZayniFramework WebAPI.Test 的測試類別
    /// </summary>
    [TestClass()]
    public class ZayniWebApiTester : MyTester
    {
        #region Private Fields

        /// <summary>失敗重試選項
        /// </summary>
        /// <returns></returns>
        private static readonly IRetryOption _retryOption = new RetryOption
        {
            EnableRetry = true,
            RetryTimes  = 3,
            RetryPeriod = TimeSpan.FromSeconds( 30 )
        };

        #endregion Private Fields


        #region Public Test Events

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassInitialize()]
        public static Task TestInitializeAsync( TestContext context ) 
        {
            StartTime         = DateTime.UtcNow;
            ServiceContainer  = AppContext.ServiceLocator;
            NotificationTilte = "[API Monitor] ZayniFramework WebAPI.Test.API.";
            return Task.FromResult( 0 );
        }

        /// <summary>測試類別清理
        /// </summary>
        /// <param name="context">測試上下文物件</param>
        /// <returns></returns>
        [ClassCleanup()]
        public static async Task TestCleanupAsync() 
        {
            EndTime = DateTime.UtcNow;
            GetTestReport( typeof ( ZayniWebApiTester ).FullName );
            Notify();
            await Task.Delay( 1000 * 3 );
        }

        #endregion Public Test Events


        #region Public Test Methods

        /// <summary>HttpClientExtension 發送 Http GET 請求測試
        /// </summary>
        [MyTestMethod()]
        [Description( "HttpClientExtension 發送 Http GET 請求測試" )]
        public async Task ExecuteGetAsync_Test()
        {
            var baseUrl    = ZayniConfigManagement.AppSettings[ "ZayniFramework-WebAPI.Test-BaseUrl" ];
            var httpClient = HttpClientFactory.Create( baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var random = new Random( Guid.NewGuid().GetHashCode() );

            var request = new HttpRequest() 
            {
                Path         = "values",
                QueryStrings = 
                {
                    [ "msg" ]    = "HttpClientExtensionTester sending http GET.",
                    [ "number" ] = random.Next( 500 ) + ""
                }
            };

            for ( int i = 0; i < 5; i++ )
            {
                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( g.Data );
                Debug.Print( g.Data );

                dynamic r = JsonConvert.DeserializeObject( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( (bool)r.success );

                await Task.Delay( 10 );
            }

            // ===================

            baseUrl    += "values/";
            httpClient = HttpClientFactory.Create( baseUrl, 5 );

            request = new HttpRequest() 
            {
                // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
                Path         = "",
                QueryStrings = 
                {
                    [ "msg" ]    = "HttpClientExtensionTester sending http GET.",
                    [ "number" ] = random.Next( 500 ) + ""
                }
            };

            for ( int i = 0; i < 5; i++ )
            {
                // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
                When.True( 3 == i, () => request.Path = null );

                var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( g.Success );
                Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( g.Data );
                Debug.Print( g.Data );

                dynamic r = JsonConvert.DeserializeObject( g.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( (bool)r.success );

                await Task.Delay( 10 );
            }
        }

        /// <summary>HttpClientExtension 發送 Http POST JSON 請求測試
        /// </summary>
        [MyTestMethod()]
        [Description( "HttpClientExtension 發送 Http POST JSON 請求測試" )]
        public async Task ExecutePostJsonAsync_Test()
        {
            var baseUrl    = ZayniConfigManagement.AppSettings[ "ZayniFramework-WebAPI.Test-BaseUrl" ];
            var httpClient = HttpClientFactory.Create( baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var random = new Random( Guid.NewGuid().GetHashCode() );

            var reqDTO = new
            {
                someMsg     = "HttpClientExtensionTester sending http POST.",
                someDate    = DateTime.UtcNow,
                magicNumber = random.Next( 300 ),
                luckyNumber = random.Next( 500 ),
                isSuperXMan = true
            };

            string reqJson = JsonConvert.SerializeObject( reqDTO );

            var request = new HttpRequest() 
            {
                Path = "/values/test",
                Body = reqJson
            };

            for ( int i = 0; i < 5; i++ )
            {
                var p = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( p.Success );
                Assert.IsTrue( p.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( p.Data );
                Debug.Print( p.Data );

                dynamic r = JsonConvert.DeserializeObject( p.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( (bool)r.success );

                await Task.Delay( 10 );
            }

            // =================

            baseUrl    += "values/test/";
            httpClient  = HttpClientFactory.Create( baseUrl, 5 );

            request = new HttpRequest() 
            {
                // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
                Path = null,
                Body = reqJson
            };

            for ( int i = 0; i < 5; i++ )
            {
                // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
                When.True( 3 == i, () => request.Path = string.Empty );

                var p = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
                Assert.IsTrue( p.Success );
                Assert.IsTrue( p.StatusCode == HttpStatusCode.OK );
                await CommandAsync.StdoutAsync( p.Data );
                Debug.Print( p.Data );

                dynamic r = JsonConvert.DeserializeObject( p.Data );
                Assert.IsNotNull( r );
                Assert.IsTrue( (bool)r.success );

                await Task.Delay( 10 );
            }
        }

        /// <summary>HttpClientExtension 的 Execute 擴充方法測試
        /// </summary>
        [MyTestMethod()]
        [Description( "HttpClientExtension 的 Execute 擴充方法測試" )]
        public async Task HttpClientExtension_Test()
        {
            var baseUrl    = ZayniConfigManagement.AppSettings[ "ZayniFramework-WebAPI.Test-BaseUrl" ];
            var httpClient = HttpClientFactory.Create( baseUrl, 5 );
            Assert.IsNotNull( httpClient );

            var random = new Random( Guid.NewGuid().GetHashCode() );

            #region 新增資料測試 Http POST Test

            var model1 = new 
            {
                name     = $"UnitTest-{RandomTextHelper.Create( 10 )}",
                sex      = 0,
                age      = 25,
                birthday = new DateTime( 1998, 3, 27 ),
                is_vip   = true,
                is_good  = true
            };

            var request = new HttpRequest() 
            {
                Path = "/users",
                Body = JsonConvert.SerializeObject( model1 )
            };

            var c = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( c.Success );
            Assert.IsTrue( c.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( c.Data );
            Debug.Print( c.Data );
            dynamic u1 = JsonConvert.DeserializeObject( c.Data );
            await Task.Delay( 100 );

            // ===============

            var model2 = new 
            {
                name     = $"UnitTest-{RandomTextHelper.Create( 10 )}",
                sex      = 1,
                age      = 30,
                birthday = new DateTime( 1998, 3, 27 ),
                is_vip   = false,
                is_good  = true
            };

            request = new HttpRequest() 
            {
                Path = "users",
                Body = JsonConvert.SerializeObject( model2 )
            };

            var c2 = await httpClient.ExecuteHttp2PostJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( c2.Success );
            Assert.IsTrue( c2.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( c2.Data );
            Debug.Print( c2.Data );
            dynamic u2= JsonConvert.DeserializeObject( c2.Data );
            await Task.Delay( 100 );

            #endregion 新增資料測試 Http POST Test

            #region 查詢資料測試 Http GET Test

            request = new HttpRequest() 
            {
                Path         = "users",
                QueryStrings = 
                {
                    [ "sex" ] = "0"
                }
            };

            var g = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( g.Success );
            Assert.IsTrue( g.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( g.Data );
            Debug.Print( g.Data );

            dynamic r = JsonConvert.DeserializeObject( g.Data );
            Assert.IsNotNull( r );
            Assert.IsTrue( (bool)r.success );

            await Task.Delay( 100 );

            // ========

            request = new HttpRequest() 
            {
                Path         = "users",
                QueryStrings = 
                {
                    [ "sex" ] = "1"
                }
            };

            var q = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( q.Success );
            Assert.IsTrue( q.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( q.Data );
            Debug.Print( q.Data );

            dynamic z = JsonConvert.DeserializeObject( q.Data );
            Assert.IsNotNull( z );
            Assert.IsTrue( (bool)z.success );

            await Task.Delay( 100 );

            // ============================

            request = new HttpRequest() 
            {
                Path = $"/users/{u1.data.account_id}"
            };

            var q1 = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( q1.Success );
            Assert.IsTrue( q1.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( q1.Data );
            Debug.Print( q1.Data );

            request = new HttpRequest() 
            {
                Path = $"/users/{u2.data.account_id}"
            };

            var q2 = await httpClient.ExecuteHttp2GetAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( q2.Success );
            Assert.IsTrue( q2.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( q2.Data );
            Debug.Print( q2.Data );

            #endregion 查詢資料測試 Http GET Test

            #region 更新資料測試 Http PUT Test

            var m1 = new 
            {
                account_id = u1.data.account_id,
                name       = "Emily Joyce",
                sex        = 0,
                age        = 22,
                birthday   = new DateTime( 2000, 5, 5 )
            };

            request = new HttpRequest() 
            {
                Path = "/users",
                Body = JsonConvert.SerializeObject( m1 )
            };

            var d1 = await httpClient.ExecuteHttp2PutJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( d1.Success );
            Assert.IsTrue( d1.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( d1.Data );
            Debug.Print( d1.Data );
            await Task.Delay( 100 );

            var m2 = new 
            {
                account_id = u2.data.account_id,
                name       = "Nancy Wilson",
                sex        = 1,
                age        = 40,
                birthday   = new DateTime( 1980, 6, 6 )
            };

            request = new HttpRequest() 
            {
                Path = "/users",
                Body = JsonConvert.SerializeObject( m2 )
            };

            var d2 = await httpClient.ExecuteHttp2PutJsonAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( d2.Success );
            Assert.IsTrue( d2.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( d2.Data );
            Debug.Print( d1.Data );
            await Task.Delay( 100 );

            #endregion 更新資料測試 Http PUT Test

            #region 資料刪除測試 Http DELETE Test

            request = new HttpRequest() 
            {
                Path = $"/users/{u1.data.account_id}"
            };

            var k1 = await httpClient.ExecuteHttp2DeleteAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( k1.Success );
            Assert.IsTrue( k1.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( k1.Data );
            Debug.Print( k1.Data );

            request = new HttpRequest() 
            {
                Path = $"/users/{u2.data.account_id}"
            };

            var k2 = await httpClient.ExecuteHttp2DeleteAsync( request, consoleOutputLog: false, withHttp2: true, httpRetryOption: _retryOption );
            Assert.IsTrue( k2.Success );
            Assert.IsTrue( k2.StatusCode == HttpStatusCode.OK );
            await CommandAsync.StdoutAsync( k2.Data );
            Debug.Print( k2.Data );

            #endregion 資料刪除測試 Http DELETE Test
        }

        #endregion Public Test Methods
    }
}
