using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace API.Monitor.Test
{
    /// <summary>測試方法標記類別
    /// </summary>
    public class MyTestMethodAttribute : TestMethodAttribute
    {
        /// <summary>執行測試方法
        /// </summary>
        /// <param name="testMethod">測試方法</param>
        /// <returns>測試結果集合</returns>
        public override TestResult[] Execute( ITestMethod testMethod )
        {
            TestResult[] results = base.Execute( testMethod );
            TestResultCollection.Results.Add( testMethod, results );
            return results;
        }
    }
}