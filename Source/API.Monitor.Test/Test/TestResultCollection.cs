using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;


namespace API.Monitor.Test
{
    /// <summary>測試結果集合
    /// </summary>
    public static class TestResultCollection
    {
        /// <summary>
        /// </summary>
        /// <typeparam name="ITestMethod"></typeparam>
        /// <typeparam name="TestResult[]"></typeparam>
        /// <returns></returns>
        public static Dictionary<ITestMethod, TestResult[]> Results { get; set; } = new Dictionary<ITestMethod, TestResult[]>();
    }
}