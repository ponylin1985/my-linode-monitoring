#!/bin/bash

find ./ -type f -name '*.nupkg' -delete
find ./ -type d -name "bin" -exec echo rm -rf {} \; && find ./ -type d -name "obj" -exec echo rm -rf {} \;

dotnet clean -c Debug
dotnet msbuild /t:Rebuild /p:Configuration=Debug /clp:Summary

find ./ -type f -name '*.nupkg' -delete